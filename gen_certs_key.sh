#!/bin/sh
# Further resources:
# - https://arminreiter.com/2022/01/create-your-own-certificate-authority-ca-using-openssl/
# - https://www.golinuxcloud.com/openssl-create-client-server-certificate/
CERTS_PATH=certs/
CERT_COUNTRY="CH"
CERT_STATE="Basel-Stadt"
CERT_LOC="Basel"
CERT_ORG="MyOrg"

CA_CERT_NAME="MyOrg Root CA"
CA_CERT_SUBJ="/CN=${CA_CERT_NAME}/C=${CERT_COUNTRY}/ST=${CERT_STATE}/L=${CERT_LOC}/O=${CERT_ORG}"

SERVER_CERT_NAME="localhost"
SERVER_CERT_SUBJ="/CN=${SERVER_CERT_NAME}/C=${CERT_COUNTRY}/ST=${CERT_STATE}/L=${CERT_LOC}/O=${CERT_ORG}"

CLIENT_CERT_NAME="MyClient"
CLIENT_CERT_SUBJ="/CN=${CLIENT_CERT_NAME}/C=${CERT_COUNTRY}/ST=${CERT_STATE}/L=${CERT_LOC}/O=${CERT_ORG}"

if [ ! -d "$CERTS_PATH" ]; then
    mkdir $CERTS_PATH
fi
cd $CERTS_PATH

# Generate CA private key
openssl genrsa -aes256 -out ca.key.pem 4096
# Create CA certificate
openssl req -x509 -new -nodes -key ca.key.pem -sha256 -days 1 -out ca.crt.pem -subj "$CA_CERT_SUBJ"

# Create certificate signing request (CSR) for server
openssl req -new -nodes -out server.csr -newkey rsa:4096 -keyout server.key.pem -subj "$SERVER_CERT_SUBJ"

# Generate v3 extension file
cat > server.v3.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
EOF

# Create server certificate
openssl x509 -req -in server.csr -CA ca.crt.pem -CAkey ca.key.pem -CAcreateserial -out server.crt.pem -days 1 -sha256 -extfile server.v3.ext

# Create certificate signing request (CSR) for client
openssl req -new -nodes -out client.csr -newkey rsa:4096 -keyout client.key.pem -subj "$CLIENT_CERT_SUBJ"

cat > client.v3.ext << EOF
basicConstraints = CA:FALSE
nsCertType = client
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
subjectAltName = DNS:localhost
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth
EOF

# Create client certificate
openssl x509 -req -in client.csr -CA ca.crt.pem -CAkey ca.key.pem -CAcreateserial -out client.crt.pem -days 1 -sha256 -extfile client.v3.ext
