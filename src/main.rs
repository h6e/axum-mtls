//! Minimal example for setting up an axum server with MTLS support.

mod cert_verifier;

use std::{fs::File, io::BufReader, path::Path, pin::Pin, sync::Arc};

use axum::{routing::get, Router};
use cert_verifier::CustomClientVerifier;
use futures_util::pin_mut;
use hyper::{body::Incoming, Request};
use hyper_util::rt::{TokioExecutor, TokioIo};
use hyper_util::server::conn::auto::Builder as ConnectionBuilder;
use rustls_pemfile::Item;
use tokio::net::TcpListener;
use tokio_rustls::rustls::server::danger::ClientCertVerifier;
use tokio_rustls::rustls::RootCertStore;
use tokio_rustls::{
    rustls::{
        pki_types::{CertificateDer, PrivateKeyDer},
        ServerConfig,
    },
    TlsAcceptor,
};
use tower_service::Service;

const ALLOWED_SUBJECTS: &[&str] = &["localhost"];

#[tokio::main]
async fn main() {
    let tls_acceptor = TlsAcceptor::from(rustls_server_config(
        "certs/server.key.pem",
        "certs/server.crt.pem",
        Some("certs/ca.crt.pem"),
    ));
    let tcp_listener = TcpListener::bind("0.0.0.0:3000")
        .await
        .expect("Could not bind tcp listener to 0.0.0.0:3000");
    let routes = axum::Router::new().route("/", get(|| async { "hello world!" }));
    pin_mut!(tcp_listener);
    loop {
        accept_connection(&routes, &tls_acceptor, &tcp_listener).await
    }
}

async fn accept_connection(
    routes: &Router,
    tls_acceptor: &TlsAcceptor,
    tcp_listener: &Pin<&mut TcpListener>,
) {
    let tower_service = routes.clone();
    let tls_acceptor = tls_acceptor.clone();
    let (cnx, addr) = tcp_listener
        .accept()
        .await
        .expect("Establishing TLS connection failed");
    tokio::spawn(async move {
        let stream = if let Ok(stream) = tls_acceptor.accept(cnx).await {
            TokioIo::new(stream)
        } else {
            println!("error during tls handshake connection from {}", addr);
            return;
        };
        let hyper_service = hyper::service::service_fn(move |req: Request<Incoming>| {
            tower_service.clone().call(req)
        });
        let ret = ConnectionBuilder::new(TokioExecutor::new())
            .serve_connection_with_upgrades(stream, hyper_service)
            .await;
        if let Err(err) = ret {
            println!("error service connection from {}: {}", addr, err)
        }
    });
}

fn rustls_server_config(
    server_key: impl AsRef<Path>,
    server_cert: impl AsRef<Path>,
    ca_cert: Option<impl AsRef<Path>>,
) -> Arc<ServerConfig> {
    let mut key_reader =
        BufReader::new(File::open(server_key).expect("Opening server key file failed"));
    let mut cert_reader =
        BufReader::new(File::open(server_cert).expect("Opening server certificate file failed"));

    let key = if let Some(Ok(k)) = rustls_pemfile::pkcs8_private_keys(&mut key_reader).next() {
        PrivateKeyDer::Pkcs8(k)
    } else {
        panic!("No valid private server key found");
    };

    let certs = rustls_pemfile::certs(&mut cert_reader)
        .filter_map(|c| c.ok())
        .collect::<Vec<CertificateDer>>();

    let mut config = if let Some(ref ca_cert) = ca_cert {
        let mut ca_reader =
            BufReader::new(File::open(ca_cert).expect("Opening ca cert file failed"));
        if let Some(Item::X509Certificate(ca)) =
            rustls_pemfile::read_one(&mut ca_reader).expect("Reading ca cert failed")
        {
            ServerConfig::builder()
                .with_client_cert_verifier(build_client_verifier(ca))
                .with_single_cert(certs, key)
                .expect("bad certificate/key")
        } else {
            panic!("invalid root cert");
        }
    } else {
        ServerConfig::builder()
            .with_no_client_auth()
            .with_single_cert(certs, key)
            .expect("bad certificate/key")
    };

    config.alpn_protocols = vec![b"h2".to_vec(), b"http/1.1".to_vec()];

    Arc::new(config)
}

fn build_client_verifier(ca_cert: CertificateDer) -> Arc<dyn ClientCertVerifier> {
    let mut root_cert_store = RootCertStore::empty();
    root_cert_store.add(ca_cert).expect("bad ca certificate");
    CustomClientVerifier::builder_with_sans(
        Arc::new(root_cert_store),
        ALLOWED_SUBJECTS
            .iter()
            .map(ToString::to_string)
            .collect::<Vec<String>>(),
    )
    .build()
    .expect("building web pki client verifier failed")
}
