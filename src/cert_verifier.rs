//! The code is based on the [`rustls::server::WebPkiClientVerifier`]

use std::sync::Arc;

use rustls_pki_types::{
    CertificateDer, CertificateRevocationListDer, DnsName, ServerName, UnixTime,
};
use tokio_rustls::rustls::client::danger::HandshakeSignatureValid;
use tokio_rustls::rustls::crypto::{
    ring, verify_tls12_signature, verify_tls13_signature, WebPkiSupportedAlgorithms,
};
use tokio_rustls::rustls::server::danger::{ClientCertVerified, ClientCertVerifier};
use tokio_rustls::rustls::server::VerifierBuilderError;
use tokio_rustls::rustls::{
    CertRevocationListError, CertificateError, DigitallySignedStruct, DistinguishedName, Error,
    OtherError, RootCertStore, SignatureScheme,
};
use webpki::{
    CertRevocationList, EndEntityCert, OwnedCertRevocationList, RevocationCheckDepth,
    UnknownStatusPolicy,
};

/// A builder for configuring a `webpki` client certificate verifier.
///
/// For more information, see the [`WebPkiClientVerifier`] documentation.
#[derive(Debug, Clone)]
pub struct ClientCertVerifierBuilder {
    roots: Arc<RootCertStore>,
    root_hint_subjects: Vec<DistinguishedName>,
    subject_alt_names: Vec<String>,
    crls: Vec<CertificateRevocationListDer<'static>>,
    revocation_check_depth: RevocationCheckDepth,
    unknown_revocation_policy: UnknownStatusPolicy,
    supported_algs: WebPkiSupportedAlgorithms,
}

impl ClientCertVerifierBuilder {
    pub(crate) fn new(
        roots: Arc<RootCertStore>,
        subject_alt_names: Vec<String>,
        supported_algs: WebPkiSupportedAlgorithms,
    ) -> Self {
        Self {
            root_hint_subjects: roots.subjects(),
            roots,
            subject_alt_names,
            crls: Vec::new(),
            revocation_check_depth: RevocationCheckDepth::Chain,
            unknown_revocation_policy: UnknownStatusPolicy::Deny,
            supported_algs,
        }
    }

    /// Build a client certificate verifier. The built verifier will be used for the server to offer
    /// client certificate authentication, to control how offered client certificates are validated,
    /// and to determine what to do with anonymous clients that do not respond to the client
    /// certificate authentication offer with a client certificate.
    ///
    /// If `with_signature_verification_algorithms` was not called on the builder, a default set of
    /// signature verification algorithms is used, controlled by the selected [`CryptoProvider`].
    ///
    /// Once built, the provided `Arc<dyn ClientCertVerifier>` can be used with a Rustls
    /// [`ServerConfig`] to configure client certificate validation using
    /// [`with_client_cert_verifier`][ConfigBuilder<ClientConfig, WantsVerifier>::with_client_cert_verifier].
    ///
    /// # Errors
    /// This function will return a `ClientCertVerifierBuilderError` if:
    /// 1. No trust anchors have been provided.
    /// 2. DER encoded CRLs have been provided that can not be parsed successfully.
    pub fn build(self) -> Result<Arc<dyn ClientCertVerifier>, VerifierBuilderError> {
        if self.roots.is_empty() {
            return Err(VerifierBuilderError::NoRootAnchors);
        }

        Ok(Arc::new(CustomClientVerifier::new(
            self.roots,
            self.root_hint_subjects,
            self.subject_alt_names,
            parse_crls(self.crls)?,
            self.revocation_check_depth,
            self.unknown_revocation_policy,
            self.supported_algs,
        )))
    }
}

#[derive(Debug)]
pub struct CustomClientVerifier {
    roots: Arc<RootCertStore>,
    root_hint_subjects: Vec<DistinguishedName>,
    subject_alt_names: Vec<String>,
    crls: Vec<CertRevocationList<'static>>,
    revocation_check_depth: RevocationCheckDepth,
    unknown_revocation_policy: UnknownStatusPolicy,
    supported_algs: WebPkiSupportedAlgorithms,
}

impl CustomClientVerifier {
    pub fn builder_with_sans(
        roots: Arc<RootCertStore>,
        subject_alt_names: Vec<String>,
    ) -> ClientCertVerifierBuilder {
        let provider = ring::default_provider();
        ClientCertVerifierBuilder::new(
            roots,
            subject_alt_names,
            provider.signature_verification_algorithms,
        )
    }

    fn new(
        roots: Arc<RootCertStore>,
        root_hint_subjects: Vec<DistinguishedName>,
        subject_alt_names: Vec<String>,
        crls: Vec<CertRevocationList<'static>>,
        revocation_check_depth: RevocationCheckDepth,
        unknown_revocation_policy: UnknownStatusPolicy,
        supported_algs: WebPkiSupportedAlgorithms,
    ) -> Self {
        Self {
            roots,
            root_hint_subjects,
            subject_alt_names,
            crls,
            revocation_check_depth,
            unknown_revocation_policy,
            supported_algs,
        }
    }
}

impl<'a> ClientCertVerifier for CustomClientVerifier {
    fn offer_client_auth(&self) -> bool {
        true
    }

    fn client_auth_mandatory(&self) -> bool {
        true
    }

    fn root_hint_subjects(&self) -> &[DistinguishedName] {
        &self.root_hint_subjects
    }

    fn verify_client_cert(
        &self,
        end_entity: &CertificateDer<'_>,
        intermediates: &[CertificateDer<'_>],
        now: UnixTime,
    ) -> Result<ClientCertVerified, Error> {
        let cert = EndEntityCert::try_from(end_entity).unwrap();

        let crl_refs = self.crls.iter().collect::<Vec<_>>();

        let revocation = if self.crls.is_empty() {
            None
        } else {
            Some(
                webpki::RevocationOptionsBuilder::new(&crl_refs)
                    // Note: safe to unwrap here - new is only fallible if no CRLs are provided
                    //       and we verify this above.
                    .unwrap()
                    .with_depth(self.revocation_check_depth)
                    .with_status_policy(self.unknown_revocation_policy)
                    .build(),
            )
        };

        let cert_has_any_san = self
            .subject_alt_names
            .iter()
            .map(|subj| ServerName::DnsName(DnsName::try_from(subj.clone()).unwrap()))
            .any(|subj| cert.verify_is_valid_for_subject_name(&subj).is_ok());

        if !cert_has_any_san {
            return Err(CertificateError::NotValidForName.into());
        }

        cert.verify_for_usage(
            self.supported_algs.all,
            &self.roots.roots,
            intermediates,
            now,
            webpki::KeyUsage::client_auth(),
            revocation,
            None,
        )
        .map_err(pki_error)
        //.map_err(pki_error)
        .map(|_| ClientCertVerified::assertion())
    }

    fn verify_tls12_signature(
        &self,
        message: &[u8],
        cert: &CertificateDer<'_>,
        dss: &DigitallySignedStruct,
    ) -> Result<HandshakeSignatureValid, Error> {
        verify_tls12_signature(message, cert, dss, &self.supported_algs)
    }

    fn verify_tls13_signature(
        &self,
        message: &[u8],
        cert: &CertificateDer<'_>,
        dss: &DigitallySignedStruct,
    ) -> Result<HandshakeSignatureValid, Error> {
        verify_tls13_signature(message, cert, dss, &self.supported_algs)
    }

    fn supported_verify_schemes(&self) -> Vec<SignatureScheme> {
        self.supported_algs.supported_schemes()
    }
}

fn parse_crls(
    crls: Vec<CertificateRevocationListDer<'_>>,
) -> Result<Vec<CertRevocationList<'_>>, CertRevocationListError> {
    crls.iter()
        .map(|der| OwnedCertRevocationList::from_der(der.as_ref()).map(Into::into))
        .collect::<Result<Vec<_>, _>>()
        .map_err(crl_error)
}

fn pki_error(error: webpki::Error) -> Error {
    use webpki::Error::*;
    match error {
        BadDer | BadDerTime | TrailingData(_) => CertificateError::BadEncoding.into(),
        CertNotValidYet => CertificateError::NotValidYet.into(),
        CertExpired | InvalidCertValidity => CertificateError::Expired.into(),
        UnknownIssuer => CertificateError::UnknownIssuer.into(),
        CertNotValidForName => CertificateError::NotValidForName.into(),
        CertRevoked => CertificateError::Revoked.into(),
        UnknownRevocationStatus => CertificateError::UnknownRevocationStatus.into(),
        IssuerNotCrlSigner => CertRevocationListError::IssuerInvalidForCrl.into(),

        InvalidSignatureForPublicKey
        | UnsupportedSignatureAlgorithm
        | UnsupportedSignatureAlgorithmForPublicKey => CertificateError::BadSignature.into(),

        InvalidCrlSignatureForPublicKey
        | UnsupportedCrlSignatureAlgorithm
        | UnsupportedCrlSignatureAlgorithmForPublicKey => {
            CertRevocationListError::BadSignature.into()
        }

        _ => CertificateError::Other(OtherError(Arc::new(error))).into(),
    }
}

fn crl_error(e: webpki::Error) -> CertRevocationListError {
    use webpki::Error::*;
    match e {
        InvalidCrlSignatureForPublicKey
        | UnsupportedCrlSignatureAlgorithm
        | UnsupportedCrlSignatureAlgorithmForPublicKey => CertRevocationListError::BadSignature,
        InvalidCrlNumber => CertRevocationListError::InvalidCrlNumber,
        InvalidSerialNumber => CertRevocationListError::InvalidRevokedCertSerialNumber,
        IssuerNotCrlSigner => CertRevocationListError::IssuerInvalidForCrl,
        MalformedExtensions | BadDer | BadDerTime => CertRevocationListError::ParseError,
        UnsupportedCriticalExtension => CertRevocationListError::UnsupportedCriticalExtension,
        UnsupportedCrlVersion => CertRevocationListError::UnsupportedCrlVersion,
        UnsupportedDeltaCrl => CertRevocationListError::UnsupportedDeltaCrl,
        UnsupportedIndirectCrl => CertRevocationListError::UnsupportedIndirectCrl,
        UnsupportedRevocationReason => CertRevocationListError::UnsupportedRevocationReason,

        _ => CertRevocationListError::Other(OtherError(Arc::new(e))),
    }
}
