# Mutual TLS with Axum Web Server Example

This is an example application providing mutual TLS with Axum. 

1. Use [`gen_certs_key.sh`](./gen_certs_key.sh) to generate keys and certificates for CA, server and client.
2. `cargo run`
3. `curl https://localhost:3000 --cacert certs/ca.crt.pem --cert certs/client.crt.pem --key certs/client.key.pem`

## Further resources

See

- https://github.com/tokio-rs/axum/blob/main/examples/low-level-rustls/src/main.rs
- https://github.com/ttys3/rust-mtls-axum-example/blob/main/src/main.rs

## Alternatives

Instead of using this low-level approach, an implementation with [`axum_server`](https://docs.rs/axum-server/latest/axum_server/) is also possible. See https://docs.rs/axum-server/latest/axum_server/ for hints.

## TODOs

- Do advanced checks with certificate (e.g. check if SANs and request domain match)
